<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable,
        CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'roles_id', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function roles()
    {
        return $this->belongsTo('App\Roles');
    }

    public function haveAccess()
    {
        $access = json_decode(Auth::user()->roles->access);
        $isAdmin = false;
        $haveAccess = false;
        
        
        
        if($isAdmin || $haveAccess){
            return true;
        }
        return true;
    }

    public function isAdmin()
    {
        $access = Auth::user()->roles->is_admin;
        
        if($access == 1){
            return true;
        }else{
            return false;
        }
    }

}
