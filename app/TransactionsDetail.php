<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionsDetail extends Model
{
    public function transaction()
    {
        return $this->belongsTo('App\Transactions');
    }
    
    public function accounts()
    {
        return $this->belongsTo ('App\Accounts','acc_id');
    }
}
