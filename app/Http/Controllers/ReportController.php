<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Penjualan;
use App\Department;
use Aivo\Highchart\Series\ScatterSeries;
use App\Http\Controllers\Controller;
use \Log;
use \Auth;
use \DB;

class ReportController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //----------------------penjualan keseluruhan-------------------------------//
        $d = array();
        $fullDate = [];
        $data = [];
        $collection = 0;
        for ($i = 0; $i < 30; $i++) {
            $d[] = date("d", strtotime('-' . $i . ' days'));
            $fullDate[] = date("d M y", strtotime('-' . $i . ' days'));
            $a = date('Y-m-d', strtotime('-' . $i . ' days'));
            $jualan = Penjualan::where('transactions_date', '=', $a)->get();
            foreach ($jualan as $key => $val) {
                $collection += $val->setor;
            }
            $data[$i] = $collection;
            $collection = 0;
        }

        $yourFirstChart["chart"] = array("type" => "line");
        $yourFirstChart["title"] = array("text" => "Grafik Penjualan Seluruh Outlet 30 Hari Terakhir");
        $yourFirstChart["xAxis"] = array("categories" => $fullDate);
        $yourFirstChart["yAxis"] = array(
            "title" => array("text" => "Terjual (Rp.)"),
            "tickInterval" => 10000000
            );
        $yourFirstChart["series"] = [
            array("name" => "Penjualan", "data" => $data),
//            array("name" => "John", "data" => [5, 7, 3])
        ];
        //---------------------------------------------------------------------//
        //
        //
        //---------------------penjualan per outlet----------------------------//

        $day = [];
        $fullDateYear = [];
        $datas = [];
        $series = [];

//        Log::debug(Auth::user()->isAdmin());
        if (Auth::user()->isAdmin()) {
            $outlet = Department::all();
        } else {
            $outlet = Department::where('user_id', Auth::user()->id)->get();
        }
        $isi = 0;
        foreach ($outlet as $keys => $value) {

            for ($i = 0; $i < 30; $i++) {
                $day[] = date("d", strtotime('-' . $i . ' days'));
                $fullDateYear[] = date("d M y", strtotime('-' . $i . ' days'));
                $a = date('Y-m-d', strtotime('-' . $i . ' days'));
                $jualan = Penjualan::selectRaw('department_id, transactions_date,sum(setor) as sumSetor')
                        ->where('transactions_date', $a)
                        ->where('department_id', $value->id)
                        ->groupBy('department_id')
                        ->get();
                foreach ($jualan as $key => $val) {
                    $isi = $val->sumSetor;
                }
                $series[$keys]['name'] = $value->name;
                $series[$keys]['data'][$i] = (int) $isi;
                $isi = 0;
            }
        }
        $secondChart["chart"] = array("type" => "line");
        $secondChart["title"] = array("text" => "Grafik Penjualan Per Outlet 30 Hari Terakhir");
        $secondChart["xAxis"] = array("categories" => $fullDateYear);
        $secondChart["yAxis"] = array(
            "title" => array("text" => "Terjual (Rp.)"),
            "tickInterval" => 10000000
            );

        $secondChart["series"] = $series;


        return view('report.index')
                        ->with('yourFirstChart', $yourFirstChart)
                        ->with('secondChart', $secondChart);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    public function transaction(Requests\ReportRequest $request, $id)
    {
//        Log::debug($request);
        $transactions = [];
        $tanggal = (!empty($request->input('tanggal'))) ? date('Y-m-d', strtotime($request->input('tanggal'))) : "";
        if (!empty($request->input('tanggal'))) {
            $transDet = DB::table('transactions_details')
                    ->join('transactions', 'transactions.id', '=', 'transactions_details.transactions_id')
                    ->join('accounts', 'accounts.id', '=', 'transactions_details.acc_id')
                    ->where('transactions.transactions_date',$tanggal)
                    ->select('transactions_details.*','accounts.name as acc_name','accounts.code as acc_code')
                    ->orderBy('transactions_details.id','DESC')
                    ->get();
            if(!empty($transDet)){
                $transactions = $transDet;
            }
            
        }
        

        return view('report.transaction')->with('transactions', $transactions);
    }
    
    public function penjualan(Requests\ReportRequest $request, $id)
    {
//        Log::debug($request);
        $transactions = [];
        $tanggal = (!empty($request->input('tanggal'))) ? date('Y-m-d', strtotime($request->input('tanggal'))) : "";
        if (!empty($request->input('tanggal'))) {
            $transDet = DB::table('penjualan_details')
                    ->join('penjualan', 'penjualan.id', '=', 'penjualan_details.penjualan_id')
                    ->join('items', 'items.id', '=', 'penjualan_details.item_id')
                    ->join('departments', 'departments.id', '=', 'penjualan.department_id')
                    ->where('penjualan.transactions_date',$tanggal)
                    ->where('departments.tipe','outlet')
                    ->select('penjualan_details.*','penjualan.setor as setor','departments.name as outlet', 'items.item_name as nama_barang')
                    ->orderBy('penjualan_details.id','DESC')
                    ->get();
            if(!empty($transDet)){
                $transactions = $transDet;
            }
            
        }

        return view('report.penjualan')->with('transactions', $transactions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
