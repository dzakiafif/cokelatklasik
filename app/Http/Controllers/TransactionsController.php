<?php

namespace App\Http\Controllers;

use App\Transactions;
use Illuminate\Http\Request;
use App\Http\Requests\TransactionsRequest;
use App\Http\Requests;
use \Auth,
    \Log,
    \Response;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{

    public function __construct()
    {
        $this->middleware('roles');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $transactions = Transactions::all();
        return view('transactions.index')->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(TransactionsRequest $request)
    {
        //simpan baru
        $params = json_decode($request->getContent());
        $form = $params->form;
        $detail = $params->detail;

        $lastCode = Transactions::orderBy('code', 'desc')->first();
        $code = '';
        if (empty($lastCode)) {
            $code = 'TRANS000001';
        } else {
            $urut = ((int) substr($lastCode->code, -6)) + 1;
            $code = 'TRANS' . substr('000000' . $urut, -6);
        }
        $model = new Transactions;
        $model->code = $code;
        $model->transactions_date = date('Y-m-d');
        $model->user_id = Auth::user()->id;
        if ($model->save()) {
            foreach ($detail as $key => $val) {
                $detail = new \App\TransactionsDetail;
                $detail->transactions_id = $model->id;
                $detail->acc_id = $val->acc_id;
                $detail->debet = $val->debet;
                $detail->kredit = $val->kredit;
                $detail->description = $val->description;

                $detail->save();
            }

            echo json_encode(array('status' => 1), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0), JSON_PRETTY_PRINT);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(TransactionsRequest $request)
    {
        //
        $params = json_decode($request);
        $data = \App\TransactionsDetail::
                where('transactions_id', $params->id)
                ->get();
        return Response::json($data);
    }

    public function view($id)
    {
        //
        $data = \App\TransactionsDetail::
                where('transactions_id', $id)
                ->get();

        foreach ($data as $key => $val) {
            $data[$key]['account_name'] = $val->accounts->name;
        }
        return Response::json($data);
    }

//    public function dataGet(TransactionsRequest $request, $id)
//    {
//        
//        $data = new \App\TransactionsDetail();
//        $data->add_detail = $request->attributes;
//        
//        
//        if ($data->save()) {
//            $this->view('transactions.index')->with('data_model');
//        }
//    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(TransactionsRequest $request, $id)
    {
        //update data
        $params = json_decode($request->getContent());
        $form = $params->form;
        $detail = $params->detail;


        //cari model
        $model = Transactions::find($id);
        $model->transactions_date = date('Y-m-d');
        $model->user_id = Auth::user()->id;
        if ($model->save()) {
            foreach ($detail as $key => $val) {

                //cari child model
                $detail = \App\TransactionsDetail::find($val->id);

                //kalo ngga ada maka create new.
                if (empty($detail))
                    $detail = new \App\TransactionsDetail;

                $detail->transactions_id = $model->id;
                $detail->acc_id = $val->acc_id;
                $detail->debet = $val->debet;
                $detail->kredit = $val->kredit;
                $detail->description = $val->description;

                $detail->save();
            }

            echo json_encode(array('status' => 1), JSON_PRETTY_PRINT);
        } else {
            echo json_encode(array('status' => 0), JSON_PRETTY_PRINT);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
