(function () {
    var app = angular.module('cokelatklasik', []);
    app.factory("Data", ['$http', '$location',
        function ($http, $q, $location) {
            var serviceBase = '/';

            var obj = {};

            obj.base = serviceBase;

            obj.get = function (q, object) {
                return $http.get(serviceBase + q, {
                    params: object
                }).then(function (results) {
                    return results.data;
                });
            };
            obj.post = function (q, object) {
                $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                return $http.post(serviceBase + q, object).then(function (results) {
                    return results.data;
                });
            };
            obj.postJson = function (q, object) {
                $http.defaults.headers.post["Content-Type"] = "application/json";
                return $http.post(serviceBase + q, object).then(function (results) {
                    return results.data;
                });
            };
            obj.put = function (q, object) {
                return $http.put(serviceBase + q, object).then(function (results) {
                    return results.data;
                });
            };
            obj.delete = function (q) {
                return $http.delete(serviceBase + q).then(function (results) {
                    return results.data;
                });
            };
            return obj;
        }]);


    app.controller("penjualanCtrl", ['$scope', '$http', function ($scope, $http, Data) {
            $scope.form = {};

            //init data;
            var tableStateRef;
            var paramRef;
            $scope.displayed = [];
            $scope.is_edit = false;
            $scope.is_view = false;
            $scope.is_create = false;
            $scope.is_newcopy = false;
            $scope.is_print = false;
            $scope.nikSkarang = '';

            $scope.callServer = function callServer(tableState) {
//                tableStateRef = tableState;
                $scope.isLoading = true;
//                var offset = tableState.pagination.start || 0;
//                var limit = tableState.pagination.number || 10;
//                var param = {offset: offset, limit: limit};
//                if (tableState.sort.predicate) {
//                    param['sort'] = tableState.sort.predicate;
//                    param['order'] = tableState.sort.reverse;
//                }
//                if (tableState.search.predicateObject) {
//                    param['filter'] = tableState.search.predicateObject;
//                }
//                paramRef = param;
                $http.get('penjualan/lists/12', {}).then(function (results) {
                    $scope.displayed = results.data;
                });
                $http.get('department/lists/12', {}).then(function (data) {
                    $scope.listDepartment = data.data;
//                    $scope.displayed = data.data;
                });
                $scope.isLoading = false;
            };
            $scope.callServer(tableStateRef);


//            var send = {};
//            var serviceBase = '/';
//            send.postJson = function (q, object) {
//                $http.defaults.headers.post["Content-Type"] = "application/json";
//                return $http.post(q, object).then(function (results) {
//                    return results.data;
//                });
//            };

            $scope.items = [];
            $http.get('api/item').success(function (data) {
                $scope.items = data;
            });
            $scope.transDetail = [];
            $scope.newsaletemp = {};
            $http.get('api/saletemp').success(function (data, status, headers, config) {
                $scope.saletemp = data;
            });
            $scope.addSaleTemp = function (item, transDetail) {
                var newDet = {
                    'id': 0,
                    'item_id': item.id,
                    'item_name': item.item_name,
                    'qty': 0,
                    'description': '',
                };
                $scope.transDetail.push(newDet);


//            $http.post('api/saletemp', { item_id: item.id, cost_price: item.cost_price, selling_price: item.selling_price }).
//            success(function(data, status, headers, config) {
//                $scope.saletemp.push(data);
//                    $http.get('api/saletemp').success(function(data) {
//                    $scope.saletemp = data;
//                    });
//            });
            };

            $scope.removeSaleTemp = function (id) {
                $http.delete('api/saletemp/' + id).
                        success(function (data, status, headers, config) {
                            $http.get('api/saletemp').success(function (data) {
                                $scope.saletemp = data;
                            });
                        });
            };

            $scope.removeRow = function (paramindex) {
                var comArr = eval($scope.transDetail);
                if (comArr.length > 1) {
                    $scope.transDetail.splice(paramindex, 1);
                } else {
                    alert("Something gone wrong");
                }
            };

            $scope.sum = function (list) {
                var total = 0;
                angular.forEach(list, function (newsaletemp) {
                    total += parseFloat(newsaletemp.item.debet * newsaletemp.quantity);
                });
                return total;
            };

            $scope.create = function (form) {
                $scope.form = {};
                $scope.transDetail = [];
                $scope.is_create = true;
                $scope.is_view = false;
                $scope.is_edit = true;
            };

            $scope.update = function (form) {
                $scope.form = form;
                $scope.transDetail = [];
                $scope.is_edit = true;
                $scope.is_create = false;
                $scope.is_view = false;
                $scope.viewDetail(form);
            };

            $scope.view = function (form) {
                $scope.form = form;
                $scope.transDetail = [];
                $scope.is_view = true;
                $scope.is_create = false;
                $scope.is_edit = true;
                $scope.viewDetail(form);
            };

            $scope.delete = function (form) {

            };

            $scope.cancle = function () {
                if (!$scope.is_view) { //hanya waktu edit cancel, di load table lagi
                    $scope.callServer(tableStateRef);
                }
                $scope.is_edit = false;
                $scope.is_view = false;
                $scope.is_print = false;
            };

            $scope.viewDetail = function (form) {
//                $http.defaults.headers.get["Content-Type"] = "application/json";
                $http.get('penjualan/view/' + form.id).then(function (results) {
//                    $scope.transDetail = results.data;
                    angular.forEach(results.data, function (val, key) {
                        $scope.transDetail.push(val);
                        $scope.transDetail[key].qty = parseFloat(val.qty);
                        $scope.transDetail[key].sisa = parseFloat(val.sisa);

                    });

                });
            };

            $scope.save = function (form, detail) {
                var data = {
                    form: form,
                    detail: detail
                };
                var obj = {};
                $http.defaults.headers.post["Content-Type"] = "application/json";
                var url = ($scope.is_create == true) ? 'penjualan/store' : 'penjualan/update/'+form.id;
                $http.post(url, data).then(function (results) {
                    if (results.data.status == 1) {
                        $scope.is_print = true;
                    } else {
                        $scope.error = true;
                    }
                });
            };

            $scope.setOutlet = function(form, id){
                angular.forEach($scope.listDepartment,function(val,key){
                    if(val.id == id){
                        form.outlet = val.name;
                    }
                });
            };

            $scope.print = function (printed) {
                    var isi = document.getElementById(printed);
                    var popupWin = window.open('', '_blank', 'width=1000,height=700');
                    popupWin.document.open()
                    popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
                    popupWin.document.close();
                
            };

        }]);
})();