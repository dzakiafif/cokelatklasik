<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_details', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('transactions_id');
            $table->foreign('transactions_id')->references('id')->on('transactions')->onDelete('restrict');
            $table->integer('acc_id')->unsigned();
            $table->foreign('acc_id')->references('id')->on('accounts')->onDelete('restrict');
            $table->integer('barang_id')->unsigned()->nullable();
            $table->foreign('barang_id')->references('id')->on('items')->onDelete('restrict');
            $table->double('debet',15,2);
            $table->double('kredit',15,2);
            $table->text('description');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions_details');
    }
}
