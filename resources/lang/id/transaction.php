<?php

return [

    'transactions_register' => 'Daftar transaksi',
    'new_transaction'       => 'Transaksi Baru',
    'item_id'               => 'ID Barang',
    'acc_id'                => 'Account',
    'barang_id'             => 'nama barang',
    'transactions_id'       => 'nama transaksi',
    'debet'                 => 'debet',
    'kredit'                => 'kredit',
    'account_id'            => 'Nama Account',
    'description'           => 'description',
    'edit'                  => 'Ganti',
    'delete'                => 'Hapus',
    'update_transaction'    => 'Ubah transaksi',
    'submit'                => 'Submit',
    'acc_id'                => 'Akun',
    'penjualan'             => 'penjualan',
    'new_penjualan'         => 'penjualan baru'

];
