<?php

return [

	'welcome' => 'Selamat Datang di KomalTech POS.',
    'recv_good'        => 'Penerimaan Barang',
    'sell_good'        => 'Penjualan',
    'transactions' => 'Transaksi',
    'outlet' => 'Outlet',
    'partner' => 'Mitra',
    'supplier' => 'Pemasok',
    'user' => 'Pengguna',
    'access_rights' => 'Hak Akses',
	'distributions'     => 'Distribusi Barang'

];
