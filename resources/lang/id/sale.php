<?php

return [

	'sales_register' => 'Distribusi Barang',
	'search_item' => 'Cari Barang:',
	'invoice' => 'Faktur',
	'employee' => 'Pegawai',
	'payment_type' => 'Jenis Pembayaran',
	'customer' => 'Distributor',
	'item_id' => 'ID Barang',
	'item_name' => 'Nama Barang',
	'price' => 'Harga',
	'quantity' => 'Quantity',
	'total' => 'Total',
	'add_payment' => 'Setoran',
	'comments' => 'Komentar',
	'grand_total' => 'TOTAL:',
	'amount_due' => 'Tersisa',
	'submit' => 'Selesai',
	//struk
	'sale_id' => 'ID Penjualan',
	'item' => 'Barang',
	'qty' => 'Qty',
	'print' => 'Cetak',
	'new_sale' => 'Penjualan Baru'

];
