<?php

return [

	'list_department'	=> 'Daftar Outlet & Mitra',
	'new_department' 		=> 'Outlet / Mitra Baru',
	'department_id'		=> 'ID Outlet',
	'name' 				=> 'Nama',
	'email' 			=> 'Email',
	'phone' 		=> 'Nomor Telepon',
	'avatar' 			=> 'Foto',
	'choose_avatar'		=> 'Pilih Foto',
	'address'			=> 'Alamat',
	'city'				=> 'Kota',
	'state'				=> 'Provinsi',
	'zip'				=> 'Kode Pos',
	'account'			=> 'Akun',
	'submit'			=> 'Simpan',
	'tipe'			=> 'Tipe',
	'edit'	=> 'Ubah',
	'delete' => 'Hapus',
	'update_department' => 'Update Data',

];
