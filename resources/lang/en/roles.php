<?php

return [

    'list_roles'	=> 'Daftar Hak Akses',
    'new_roles' 		=> 'Hak Akses Baru',
    'id'		=> 'ID',
    'name' 				=> 'Nama',
    'access' 			=> 'access',
        'edit'	=> 'Ganti',
    'delete' => 'Hapus',
    'update_roles' => 'Ganti roles',
    'submit' => 'Submit'

];
