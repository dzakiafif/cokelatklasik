<?php

return [

    'list_customers' => 'List Mitra',
    'new_customer' => 'New Mitra',
    'customer_id' => 'Mitra ID',
    'name' => 'Name',
    'email' => 'Email',
    'phone_number' => 'Phone Number',
    'avatar' => 'Avatar',
    'choose_avatar' => 'Choose Avatar',
    'address' => 'Address',
    'city' => 'City',
    'state' => 'State',
    'zip' => 'Zip',
    'company_name' => 'Company Name',
    'account' => 'Account',
    'submit' => 'Submit',
    'is_delete' => 'Is Delete',
    'tipe' => 'Tipe',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'update_customer' => 'Update Customer',
];
