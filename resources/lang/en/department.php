<?php

return [

	'list_department'	=> 'Outlet & Partner List',
	'new_department' 		=> 'New Outlet/ Partner',
	'department_id'		=> 'ID Pelanggan',
	'name' 				=> 'Nama',
	'email' 			=> 'Email',
	'phone' 		=> 'Nomor Telepon',
	'avatar' 			=> 'Foto',
	'choose_avatar'		=> 'Pilih Foto',
	'address'			=> 'Alamat',
	'city'				=> 'Kota',
	'state'				=> 'Provinsi',
	'zip'				=> 'Kode Pos',
	'company_name'		=> 'Nama Perusahaan',
	'account'			=> 'Akun',
	'Submit'			=> 'Submit',
	'edit'	=> 'Ganti',
	'delete' => 'Hapus',
	'update_department' => 'Edit Outlet',

];
