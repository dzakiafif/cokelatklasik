<?php

return [

    'list_accounts'     	=> 'List Accounts',
    'new_accounts' 		=> 'New Account',
    'id'                        => 'ID',
    'code'      		=> 'Account Code',
    'name' 			=> 'Name',
    'access' 			=> 'Access',
    'edit'              	=> 'Change',
    'delete'                    => 'Delete',
    'update_accounts'           => 'Change accounts',
    'submit'                    => 'Submit'

];
