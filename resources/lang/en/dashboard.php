<?php

return [

	'welcome' => 'Selamat Datang di KomalTech POS.',
	'recv_good'        => 'Receiving Goods',
	'sell_good'        => 'Sell of Goods',
	'transactions' => 'Transactions',
	'outlet' => 'Outlet',
	'partner' => 'Partner',
	'supplier' => 'Supplier',
	'user' => 'User',
	'access_rights' => 'Access Rights',
	'distributions'     => 'Dist. of Goods'
];
