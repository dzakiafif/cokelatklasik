@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <h3><i class="icon icon-grid-lines-streamline"></i> {{trans('employee.list_employees')}}</h3>
            <div class="panel panel-default">

                <div class="panel-body">
                    @if (json_decode(Auth::user()->roles->access) == null)
                    <a class="btn btn-small btn-success" href="{{ URL::to('employees/create') }}">{{trans('employee.new_employee')}}</a>
                    @endif
                    <hr />
                    
                    
                    @if (Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td>{{trans('employee.person_id')}}</td>
                                <td>{{trans('employee.name')}}</td>
                                <td>{{trans('employee.email')}}</td>
                                <td>&nbsp;</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employee as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->email }}</td>
                                <td >


                                    {!! Form::open(array('url' => 'employees/' . $value->id, 'class' => 'pull-right')) !!}
                                    <a class="btn btn-small btn-info" href="{{ URL::to('employees/' . $value->id . '/edit') }}">{{trans('employee.edit')}}</a>
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::submit(trans('employee.delete'), array('class' => 'btn btn-warning')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection