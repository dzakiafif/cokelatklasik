@extends('app')
@section('content')
<br>
<div class="row little">
    <div class="col-sm-4">
        <p>
            <a href="{{ url('/sales')}}" class="btn btn-h2">
                <img src="{{asset('assets/img/icon%20konversi.png')}}">
                <span class="inner-font">{{ trans('dashboard.distributions') }}
                </span>
            </a>
        </p>
    </div>
    <div class="col-sm-4">
        <p>
            <a href="{{ url('/receivings')}}" class="btn btn-h3">
                <img src="{{asset('assets/img/icon%20bahan.png')}}">
                <span class="inner-font">
                    {{ trans('dashboard.recv_good') }}
                </span>
            </a>
        </p>
    </div>
    <div class="col-sm-4">
        <p>
            <a href="{{ url('/penjualan')}}" class="btn btn-h4">
                <img src="{{asset('assets/img/icon%20keuangan.png')}}">
                <span class="inner-font">
                    {{ trans('dashboard.sell_good') }}
                </span>
            </a>
        </p>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <p>
            <a href="{{ url('/accounts')}}" class="btn btn-h5">
                <img src="{{asset('assets/img/50355-200.png')}}">
                <span class="inner-font">
                    {{ trans('accounts.list_accounts') }}
                </span>
            </a>
        </p>
    </div>
    {{-- <div class="col-sm-4">
        <img src="{{ asset('assets/img/mastej-logo.png') }}" height="135" style="padding-left: 65px" />
    </div> --}}
    <div class="col-sm-4">
        <p>
            <a href="{{ url('/department')}}" class="btn btn-h1">
                <img src="{{ asset('assets/img/icon%20pelanggan.png')}}">
                <span class="inner-font">
                    Outlet &amp; Mitra
                </span>
            </a>
        </p>
    </div>
    <div class="col-sm-4">
        <p>
            <a href="{{url('/suppliers')}}" class="btn btn-h5">
                <img src="{{asset('assets/img/icon%20menu.png')}}">
                <span class="inner-font">
                    {{ trans('dashboard.supplier') }}
                </span>
            </a>
        </p>
    </div>
    <!--        <div class="col-sm-4">
        <p>-->
            <!--<a href="{{ url('/customers')}}" class="btn btn-h5">-->
            <!--                    <img src="{{ asset('assets/img/icon%20pelanggan.png') }}">
            <span class="inner-font">
                Outlet & Partnership
            </span>-->
        <!--                </a>
    </p>
</div>-->
</div>
@endsection