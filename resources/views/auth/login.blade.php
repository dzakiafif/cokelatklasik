<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cokelat klasik - login</title>
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/css/global_style.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
<div id="loginmodal" class="login-background">
	<img src="{{ asset('assets/img/logo.png') }}" class="img-responsive" style="margin-left: 20%; margin-top: 4%;">
	<div class="loginmodal-container">
		<h4><b>LOG IN</b></h4>
		<h5>please fill in your basic info:</h5>
		<br>
		<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

			{{--<input type="text" name="user" placeholder="Username">--}}
				<input type="text" placeholder="username" class="form-control" name="username" value="{{ old('username') }}">
			<input type="password" name="password" placeholder="Password">
			{{--<input type="checkbox" name="reminder"><span style="margin-left: 5px;">Remember Me</span>&nbsp;|&nbsp;--}}
				{{--<a href="{{ url('/password/email') }}"><span>Forgot Password</span></a>--}}
			<input type="submit" name="login" class="login loginmodal-submit" value="LOG IN">


		</form>

	</div>
</div>

</body>
<script type="text/javascript" src="assets/js/jquery-2.1.4.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
</html>




