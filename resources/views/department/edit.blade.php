@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-11">
			<h3><i class="icon icon-grid-lines-streamline"></i> {{trans('department.update_department')}}</h3>
			<div class="panel panel-default">
				{{--<div class="panel-heading">{{trans('department.update_department')}}</div>--}}

				<div class="panel-body">
					{!! Html::ul($errors->all()) !!}

					{!! Form::model($department, array('route' => array('department.update', $department->id), 'method' => 'PUT', 'files' => true)) !!}
					<div class="form-group">
					{!! Form::label('name', trans('department.name')) !!}
					{!! Form::text('name', Input::old('name'), array('class' => 'form-control')) !!}
					</div>
					<div class="form-group">
						{!! Form::label('tipe', trans('department.tipe')) !!}
						{!! Form::select('tipe', array('outlet' => 'outlet', 'mitra' => 'mitra'), Input::old('tipe'), array('class' => 'form-control')) !!}

					</div>

					<div class="form-group">
					{!! Form::label('email', trans('department.email')) !!}
					{!! Form::text('email', Input::old('email'), array('class' => 'form-control')) !!}
					</div>

					<div class="form-group">
					{!! Form::label('phone', trans('department.phone')) !!}
					{!! Form::text('phone', Input::old('phone'), array('class' => 'form-control')) !!}
					</div>

					<div class="form-group">
					{!! Form::label('address', trans('department.address')) !!}
					{!! Form::text('address', Input::old('address'), array('class' => 'form-control')) !!}
					</div>

					{!! Form::submit(trans('department.submit'), array('class' => 'btn btn-primary')) !!}

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection