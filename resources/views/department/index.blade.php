@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-11">
            <h3><i class="icon icon-grid-lines-streamline"></i> {{trans('department.list_department')}}</h3>
            <div class="panel panel-default">
                {{--<div class="panel-heading">{{trans('department.list_department')}}</div>--}}

                <div class="panel-body">
                    <a class="btn btn-small btn-success" href="{{ URL::to('department/create') }}">New +</a>
                    <hr />
                    @if (Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td>{{trans('department.name')}}</td>
                                <td>{{trans('department.email')}}</td>
                                <td>{{trans('department.phone')}}</td>
                                <td>{{trans('department.tipe')}}</td>
                                <td>&nbsp;</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($department as $value)
                            <tr>
                                <td>{{ $value->name }}</td>
                                <td>{{ $value->email }}</td>
                                <td>{{ $value->phone }}</td>
                                <td>{{ $value->tipe}}</td> 
                                <td>


                                    {!! Form::open(array('url' => 'department/' . $value->id, 'class' => 'pull-right')) !!}
                                    <a class="btn btn-small btn-info" href="{{ URL::to('department/' . $value->id . '/edit') }}" title="{{trans('department.edit')}}"><i class="fa fa-pencil"></i></a>
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::submit(trans('department.delete'), array('class' => 'btn btn-warning')) !!}
                                    {!! Form::close() !!}
                                </td>
                                {{--<td>{!! Html::image(url() . '/images/departments/' . $value->avatar, 'a picture', array('class' => 'thumb')) !!}</td>--}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection