@extends('app')
@section('breadcrumb')
<li>Report</li>
<li><a href=""> {{trans('report-sale.sales_report')}}</a></li>
@endsection
@section('content')
<div id="printed-area">
	<div class="container-fluid" style="text-transform:capitalize">
		<div class="row">
			<div class="col-md-11">
				<h2 style="font-weight: 700; text-align: center;">COKELAT KLASIK PASURUAN</h2>
				<div>
					<h3 id="tengah"><i class="icon icon-grid-lines-streamline"></i> {{trans('report-sale.sales_report')}}</h3>
					<div class="panel-body">
						<div class="row">
							{{--<div class="col-md-4">--}}
								{{--<div class="well well-sm">{{trans('report-sale.grand_total')}}: {{DB::table('sale_items')->sum('total_selling')}}</div>--}}
							{{--</div>--}}
							{{--<div class="col-md-4">--}}
								{{--<div class="well well-sm">{{trans('report-sale.grand_profit')}}: {{DB::table('sale_items')->sum('total_selling') - DB::table('sale_items')->sum('total_cost')}}</div>--}}
							{{--</div>--}}
							<div class="col-md-10">&nbsp;</div>
							<div class="col-md-2">
								<button type="button" onclick="print('printed-area')" class="btn btn-success pull-right hidden-print" style="margin-right: 5px;width: 100%;vertical-align: middle;"><i class="fa fa-print"></i> {{trans('sale.print')}}</button>
							</div>
						</div>
						<br/>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<td>{{trans('report-sale.item_id')}}</td>
									<td>{{trans('report-sale.date')}}</td>
									<td>{{trans('report-sale.items_solded')}}</td>
									<td>{{trans('report-sale.sold_by')}}</td>
									{{--<td>{{trans('report-sale.sold_to')}}</td>--}}
									<td>{{trans('report-sale.total')}}</td>
									{{--<td>{{trans('report-sale.item_id')}}</td>--}}
									{{--<td>{{trans('report-sale.item_name')}}</td>--}}
									<td>{{trans('report-sale.quantity_sold')}}</td>
									{{--<td>{{trans('report-sale.total')}}</td>--}}
									{{--<td>{{trans('report-sale.profit')}}</td>--}}
									<td>{{trans('report-sale.payment_type')}}</td>
									<td>{{trans('report-sale.comments')}}</td>
									<td class="hidden-print">&nbsp;</td>
								</tr>
							</thead>
							<tbody>
								@foreach($saleReport as $value)
								<?php
								$department = (!empty($value->department->name)) ? $value->department->name : '';
								?>
								<tr>
									<td>{{ $value->id }}</td>
									<td>{{ $value->created_at }}</td>
									<td>{{DB::table('sale_items')->where('sale_id', $value->id)->sum('quantity')}}</td>
									<td>{{ $value->user->name }}</td>
									{{--<td style="text-transform: capitalize;">{{ $department }}</td>--}}
									<td>Rp. {{DB::table('sale_items')->where('sale_id', $value->id)->sum('total_selling')}}</td>
									<td>{{DB::table('sale_items')->where('sale_id', $value->id)->sum('total_selling') - DB::table('sale_items')->where('sale_id', $value->id)->sum('total_cost')}}</td>
									<td>{{ $value->payment_type }}</td>
									<td>{{ $value->comments }}</td>
									<td class="hidden-print">
										<a class="btn btn-small btn-info" data-toggle="collapse" href="#detailedSales{{ $value->id }}" aria-expanded="false" aria-controls="detailedReceivings">
										{{trans('report-sale.detail')}}</a>
									</td>
								</tr>
								<tr class="collapse" id="detailedSales{{ $value->id }}">
									<td colspan="10">
										<table class="table">
											<tr>
												<td>{{trans('report-sale.item_id')}}</td>
												<td>{{trans('report-sale.item_name')}}</td>
												<td>{{trans('report-sale.quantity_sold')}}</td>
												<td>{{trans('report-sale.total')}}</td>
												{{--<td>{{trans('report-sale.profit')}}</td>--}}
											</tr>
											@foreach(ReportSalesDetailed::sale_detailed($value->id) as $SaleDetailed)
											<tr>
												<td>{{ $SaleDetailed->item_id }}</td>
												<td>{{ $SaleDetailed->item->item_name }}</td>
												<td>{{ $SaleDetailed->quantity }}</td>
												<td>{{ $SaleDetailed->selling_price * $SaleDetailed->quantity}}</td>
												{{--<td>{{ ($SaleDetailed->quantity * $SaleDetailed->selling_price) - ($SaleDetailed->quantity * $SaleDetailed->cost_price)}}</td>--}}
											</tr>
											@endforeach
										</table>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
function print(printed) {
var isi = document.getElementById(printed);
var popupWin = window.open('', '_blank', 'width=1000,height=700');
popupWin.document.open()
popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/global-style.css') }}" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
popupWin.document.close();
}
</script>
@endsection