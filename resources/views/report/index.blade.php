@extends('app')
@section('content')
{!! Html::script('js/highcharts.js', array('type' => 'text/javascript')) !!}

<div class="container-fluid">
    <div class="row">
        <div class="col-md-11">
            <h3><i class="icon icon-grid-lines-streamline"></i> GRAFIK PENJUALAN</h3>
            <div class="panel panel-default">
                <div class="panel-hea">Report</div>

                <div class="panel-body">
                    <div id="container" style="width:100%; height:400px;"></div>
                    <div id="container2" style="width:100%; height:400px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type = "text/javascript" >
    $(function () {
        $('#container').highcharts(
        {!! json_encode($yourFirstChart) !!}
        );
    });
    $(function () {
        $('#container2').highcharts(
        {!! json_encode($secondChart) !!}
        );
    });
</script>
@endsection