@extends('app')
@section('breadcrumb')
<li>Report</li>
<li><a href=""> {{trans('report-sale.sales_report')}}</a></li>
@endsection
@section('content')
<div id="printed-area">
    <div class="container-fluid" style="text-transform:capitalize">
        <form method="GET">
            <div class="row">
                <div class="col-md-11">
                    <h2 style="font-weight: 700; text-align: center;">COKELAT KLASIK PASURUAN</h2>
                    <div>
                        <h3 id="tengah"><i class="icon icon-grid-lines-streamline"></i>Laporan Transaksi</h3>
                        <div class="panel-body">
                            <div class="row">
                                <div class="well well-sm col-md-12 hidden-print">
                                    <div class=" input-group">
                                        <input type="date" class="form-control col-md-4" name="tanggal"/>
                                        <input type="submit" class="btn btn-info col-md-4" name="submit" value="submit"/>
                                    </div>
                                </div>
                                <div class="col-md-10">&nbsp;</div>
                                <div class="col-md-2">
                                    <button type="button" onclick="print('printed-area')" class="btn btn-success pull-right hidden-print" style="margin-right: 5px;width: 100%;vertical-align: middle;"><i class="fa fa-print"></i> Cetak</button>
                                </div>
                            </div>
                            <br/>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <td>No.</td>
                                        <td>Kode</td>
                                        <td>Nama Akun</td>
                                        <td>Debet</td>
                                        <td>Credit</td>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $i = 1;
                                    $debet = 0;
                                    $credit = 0;
                                    $total = 0;
                                    ?>
                                    @foreach($transactions as $val)
                                    <?php
                                    
                                    ?>
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $val->acc_code}}</td>
                                        <td>{{ $val->acc_name}}</td>
                                        <td style="text-align: right">{{ $val->debet}}</td>
                                        <td style="text-align: right">{{ $val->kredit}}</td>
                                        <?php
                                        $debet += $val->debet;
                                        $credit +=  $val->kredit;
                                        $total += $val->debet - $val->kredit;
                                        ?>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="text-align: left">Total</td>
                                        <td>&nbsp;</td>
                                        <td style="text-align: right"><?= $total?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function print(printed) {
        var isi = document.getElementById(printed);
        var popupWin = window.open('', '_blank', 'width=1000,height=700');
        popupWin.document.open()
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/global-style.css') }}" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
                popupWin.document.close();
    }
</script>
@endsection
