@extends('app')
@section('breadcrumb')
<li>Report</li>
<li></li>
@endsection
@section('content')
<div id="printed-area">
    <div class="container-fluid" style="text-transform:capitalize">
        <form method="GET">
            <div class="row">
                <div class="col-md-11">
                    <h2 style="font-weight: 700; text-align: center;">COKELAT KLASIK PASURUAN</h2>
                    <div>
                        <h3 id="tengah"><i class="icon icon-grid-lines-streamline"></i>Laporan Penjualan</h3>
                        <div class="panel-body">
                            <div class="row">
                                 <div class="well well-sm col-md-12 hidden-print">
                                    <div class=" input-group">
                                    <?php
                                        $tanggal = (isset($_GET['tanggal']) && !empty($_GET['tanggal'])) ? $_GET['tanggal'] : '';
                                    ?>
                                        <input type="date" class="form-control col-md-4" name="tanggal" value="<?= $tanggal?>"/>
                                        <input type="submit" class="btn btn-info col-md-4" name="submit" value="submit"/>
                                    </div>
                                </div>
                                <div class="col-md-10">&nbsp;</div>
                                <div class="col-md-2">
                                    <button type="button"  onclick="print('printed-area')" class="btn btn-success pull-right hidden-print" style="margin-right: 5px;width: 100%;vertical-align: middle;"><i class="fa fa-print"></i> Cetak</button>
                                </div>
                            </div>
                            <br/>
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <td>No.</td>
                                        <td>Nama Barang</td>
                                        <td>Nama Outlet</td>
                                        <td>Jumlah Setor</td>
                                        <td>Terjual</td>
                                        <td>Sisa</td>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $i = 1;
                                    $qty = 0;
                                    $sisa = 0;
                                    $setor = 0;
                                    ?>

                                    @foreach($transactions as $val)

                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $val->nama_barang}}</td>
                                        <td>{{ $val->outlet}}</td>
                                        <td>{{ $val->setor}}</td>
                                        <td style="text-align: right">{{ $val->qty}}</td>
                                        <td style="text-align: right">{{ $val->sisa}}</td>

                                        <?php
                                        $qty += $val->qty;
                                        $sisa +=  $val->sisa;
                                        $setor += (float) $val->setor;
                                        ?>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3" style="text-align: left">Total</td>
                                        <td style="text-align: left"><?= $setor?></td>
                                        <td style="text-align: right"><?= (float) $qty?></td>
                                        <td style="text-align: right"><?= (float) $sisa?></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function print(printed) {
        var isi = document.getElementById(printed);
        var popupWin = window.open('', '_blank', 'width=1000,height=700');
        popupWin.document.open()
                popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="{{ asset('css/print.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" /><link rel="stylesheet" type="text/css" href="{{ asset('assets/css/global-style.css') }}" /></head><body onload="window.print();window.close();">' + isi.innerHTML + '</body></html>');
                popupWin.document.close();
    }
</script>
@endsection