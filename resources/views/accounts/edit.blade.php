@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">{{trans('accounts.update_accounts')}}</div>

                    <div class="panel-body">
                        {!! Html::ul($errors->all()) !!}

                        {!! Form::model($accounts, array('route' => array('accounts.update', $accounts->id), 'method' => 'PUT', 'files' => true)) !!}

                        <div class="form-group">
                            {!! Form::label('code', trans('accounts.code')) !!}
                            {!! Form::text('code', null, array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('name', trans('accounts.name')) !!}
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>


                        {!! Form::submit(trans('accounts.submit'), array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection