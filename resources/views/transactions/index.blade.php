@extends('app')

@section('breadcrumb')
<li><a href=""> {{trans('transaction.new_transaction')}}</a></li>
@endsection

@section('content')
{!! Html::script('js/angular.min.js', array('type' => 'text/javascript')) !!}
{!! Html::script('js/transactions.js', array('type' => 'text/javascript')) !!}

<div class="container">
    <div class="row">
        <div class="col-md-11">
            <h3><i class="icon icon-grid-lines-streamline"></i> {{trans('transaction.new_transaction')}}</h3>
            <p>&nbsp;</p>
            <div class="panel panel-default">
                {{-- < div class = "panel-heading" > < span class = "glyphicon glyphicon-inbox" aria - hidden = "true" > < /span> {{trans('transactions.transactions_register')}}</div>--}}

            <div class="panel-body">

                @if (Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
                @endif
                {!! Html::ul($errors->all()) !!}

                <div class="row" ng-controller="transCtrl">
                    <div class="panel-body" ng-hide="is_edit"  st-pipe="callServer" st-table="displayed">
                        <button class="btn btn-small btn-success" ng-click="create()" ng-show="is_edit == false">{{trans('transaction.new_transaction')}}</button>
                        <hr />

                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Kode Transaksi</td>
                                    <td>Tanggal Transaksi</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="det in displayed">
                                    <td>@{{det.id}}</td>
                                    <td>@{{det.code}}</td>
                                    <td>@{{det.transactions_date| date}}</td>
                                    <td>
                                        <button ng-click="update(det)" class="btn btn-small btn-info" >{{trans('transaction.edit')}}</button>

                                    </td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                    <div class="panel-body" ng-show="is_edit">
                        <div class="col-md-3">
                            <label>{{trans('transaction.account_id')}} <input ng-model="searchKeyword" class="form-control"></label>

                            <table class="table table-hover">
                                <tr ng-repeat="item in items| filter: searchKeyword | limitTo:10">

                                    <td>@{{item.code}}</td>
                                    <td>@{{item.name}}</td>
                                    <td>
                                        <button class="btn btn-success btn-xs" type="button" ng-click="addSaleTemp(item, newsaletemp)">
                                            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                        </button>
                                    </td>

                                </tr>
                            </table>
                        </div>

                        <div class="col-md-9">

                            <div class="row">
                                <form name="KomalForm">
                                    <!--{!! Form::open(array('url' => 'sales', 'class' => 'form-horizontal')) !!}-->

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>ID akun</th>
                                                <th>Nama Akun</th>
                                                <th>Debet</th>
                                                <th>Kredit</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="det in transDetail">
                                                <td>@{{det.acc_id}}</td>
                                                <td>@{{det.account_name}}</td>
                                                <td><input class="form-control" type="number" name="debet[]" ng-change="sumKeseluruhan(transDetail)" ng-model="det.debet" size="20" required title="Data yang anda masukkan kurang lengkap" x-moz-errormessage="Data yang anda masukkan kurang lengkap"></td>
                                                <td><input class="form-control" type="number" name="kredit[]" ng-change="sumKeseluruhan(transDetail)" ng-model="det.kredit" size="20" required title="Data yang anda masukkan kurang lengkap" x-moz-errormessage="Data yang anda masukkan kurang lengkap"></td>
                                                <td>
                                                    <button class="btn btn-danger btn-xs" type="button" ng-click="removeRow($index)">
                                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2">Total</td>
                                                <td>&nbsp;</td>
                                                <td>@{{totalKeseluruhan}}</td>
                                                <td></td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="employee" class="col-sm-4 control-label">Keterangan</label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control" ng-model="form.description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-sm-4"><button type="submit" ng-click="cancle()" class="btn ">Back</button></div>
                                                <div class="col-sm-7">
                                                    <button type="submit" ng-readonly="form.debet != form.credit" ng-click="save(form, transDetail)" class="btn btn-success btn-block"><i class="fa fa-fw fa-check"></i> {{trans('sale.submit')}}</button>
                                                </div>
                                                <div class="col-sm-1"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <!--                            {!! Form::close() !!}-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
